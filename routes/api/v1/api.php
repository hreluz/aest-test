<?php

use App\Http\Controllers\Api\v1\CategoriesController;
use App\Http\Controllers\Api\v1\ProductsController;
use App\Http\Controllers\Api\v1\UsersController;
use App\Http\Controllers\Api\v1\VendorProductsController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'        => 'v1' ,
    'namespace'     => 'ApiHeader\v1',
    'as'            => 'api.v1.',
    'middleware'    => 'api-header'
], function() {

    Route::post('users',  [UsersController::class, 'store'])
        ->name('users.store');

    Route::post('products',  [ProductsController::class, 'store'])
        ->name('products.store');

    Route::get('product/{product}',  [ProductsController::class, 'show'])
        ->name('products.show');

    Route::post('categories',  [CategoriesController::class, 'store'])
        ->name('categories.store');

    Route::get('vendor/products/rating/{vendor}',  [VendorProductsController::class, 'rating'])
        ->name('vendor.products.rating');
});
