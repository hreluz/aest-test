<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\Api\v1\Categories\StoreRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;

class CategoriesController extends BaseApiController
{
    public function store(StoreRequest $request) {

        $product =  $user = Category::create($request->only([ 'name']));

        return $this->success([
            'category' => new CategoryResource($product)
        ]);
    }
}
