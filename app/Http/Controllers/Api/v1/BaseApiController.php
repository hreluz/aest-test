<?php


namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BaseApiController extends Controller
{

    protected function success(array $data = [], string $message = null): JsonResponse
    {
        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => $message ?: 'OK',
        ]);
    }

}
