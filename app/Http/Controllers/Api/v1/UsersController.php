<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\Api\v1\Users\StoreRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;

class UsersController extends BaseApiController
{
    public function store(StoreRequest $request) {
        $user = User::create($request->only([
                'name',
                'email',
                'type'
        ]));

        return $this->success([
            'user'=> new UserResource($user)
        ]);
    }
}
