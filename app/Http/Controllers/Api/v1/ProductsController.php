<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\Api\v1\Product\StoreRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use App\Models\User;

class ProductsController extends BaseApiController
{
    public function store(StoreRequest $request) {
        $user = User::find($request->get('user_id'));
        $product = $user->products()->create($request->only([
            'name',
            'category_id',
            'description',
            'price',
            'rating'
        ]));

        return $this->success([
            'product' => new ProductResource($product)
        ]);
    }

    public function show(Product $product) {
        return $this->success([
            'product' => new ProductResource($product)
        ]);
    }
}
