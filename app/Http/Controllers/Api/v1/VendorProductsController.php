<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\Vendors\RatingsProductsRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductRatingCollection;
use App\Models\User;
use Illuminate\Http\Request;

class VendorProductsController extends BaseApiController
{
    public function rating(RatingsProductsRequest $request, User $vendor){
        $products = new ProductRatingCollection($vendor->products);

        return $this->success([
            'products' => $products,
            'total'    => count($products)
        ]);
    }
}
