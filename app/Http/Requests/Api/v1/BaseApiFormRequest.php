<?php


namespace App\Http\Requests\Api\v1;

use Illuminate\Foundation\Http\FormRequest;

class BaseApiFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
}
