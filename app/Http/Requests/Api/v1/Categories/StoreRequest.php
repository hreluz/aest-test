<?php

namespace App\Http\Requests\Api\v1\Categories;

use App\Http\Requests\Api\v1\BaseApiFormRequest;

class StoreRequest extends BaseApiFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|unique:categories,name'
        ];
    }
}
