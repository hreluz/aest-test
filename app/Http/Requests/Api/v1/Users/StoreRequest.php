<?php

namespace App\Http\Requests\Api\v1\Users;

use App\Http\Requests\Api\v1\BaseApiFormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;

class StoreRequest extends BaseApiFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'email' => 'required|email',
            'type'  => 'required|'. Rule::in([User::TYPE_VENDOR, User::TYPE_BUYER]),
        ];
    }
}
