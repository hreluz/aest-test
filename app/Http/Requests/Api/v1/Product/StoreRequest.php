<?php

namespace App\Http\Requests\Api\v1\Product;

use App\Http\Requests\Api\v1\BaseApiFormRequest;
use App\Models\User;

class StoreRequest extends BaseApiFormRequest
{
    public function authorize() {
        $user = User::find($this->get('user_id'));
        return $user ? $user->isVendor : false;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'category_id'   => 'required|exists:categories,id',
            'description'   => 'required',
            'price'         => 'required|numeric'
        ];
    }
}
