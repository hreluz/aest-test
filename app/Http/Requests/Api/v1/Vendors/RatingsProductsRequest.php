<?php

namespace App\Http\Requests\Api\v1\Vendors;

use App\Http\Requests\Api\v1\BaseApiFormRequest;

class RatingsProductsRequest extends BaseApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->vendor ? $this->vendor->isVendor : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
