<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const TYPE_VENDOR = 1;
    const TYPE_BUYER  = 2;

    const TYPE_NAMES = [
        self::TYPE_VENDOR => 'vendor',
        self::TYPE_BUYER  => 'buyer'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'type'
    ];

    public function getTypeNameAttribute() {
        return self::TYPE_NAMES[$this->type];
    }

    /**
     * Relationships
     */
    public function products() {
        return $this->hasMany(Product::class);
    }

    /**
     * Custom Attributes
     */
    public function getIsVendorAttribute() {
        return self::TYPE_VENDOR == $this->type;
    }
}
