<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'      => $this->faker->name,
            'price'     => $this->faker->randomFloat(2, 1, 1000),
            'description' => $this->faker->text,
            'rating'      => rand(1,5),
            'category_id'  => function() {
                return Category::factory()->create()->id;
            },
            'user_id'  => function() {
                return User::factory()->create([
                    'type' => User::TYPE_VENDOR
                ])->id;
            }
        ];
    }
}
