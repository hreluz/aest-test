<?php

namespace Tests\Feature\Api;

use App\Models\Product;

class VendorsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_shows_ratings_of_products() {
        $vendor = $this->createVendor();
        $data = Product::factory()->make()->toArray();

        Product::factory()->count(5)->create(array_merge(
            $data,
            ['user_id' => $vendor->id]
        ));

        $response = $this->get(route('api.v1.vendor.products.rating', $vendor->id));
        $response->assertOk();
        $response->assertJsonFragment(['total' => 5]);
        $response->assertJsonStructure([
            "data" => [
                'products' => [
                    '*' => [
                        'name',
                        'rating',
                    ]
                ]
            ]
        ]);
    }

    /**
     *
     */
    public function it_shows_unauthorized_if_user_is_not_vendor() {
        $buyer = $this->createBuyer();
        $response = $this->get(route('api.v1.vendor.products.rating', $buyer->id));
        $response->assertStatus(403);
    }
}
