<?php

namespace Tests\Feature\Api;

use App\Models\Product;
use function Symfony\Component\String\u;

class ProductsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_can_create_a_product_if_user_vendor_is_sent() {
        $data = Product::factory()->make()->toArray();
        $response = $this->post(route('api.v1.products.store'), array_merge($data, [
            'user_id' => $this->createVendor()->id
        ]));
        $response->assertOk();

        unset($data['user_id']);
        $response->assertJsonFragment($data);
        $this->assertDatabaseHas('products', $data);
    }

    /**
     * @test
     */
    public function it_doesnt_allow_to_create_product_with_empty_field()
    {
        $response = $this->post(route('api.v1.products.store'), [
            'user_id' => $this->createVendor()->id
        ]);
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function it_shows_unauthorized_if_user_is_not_sent()
    {
        $response = $this->post(route('api.v1.products.store'), []);
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function it_shows_unauthorized_if_user_is_not_vendor()
    {
        $response = $this->post(route('api.v1.products.store'), [
            'user_id' => $this->createBuyer()->id
        ]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function it_does_not_allow_to_create_with_wrong_category()
    {
        $data = Product::factory()->make()->toArray();

        $response = $this->post(route('api.v1.products.store'), array_merge($data, [
            'user_id' => $this->createVendor()->id,
            'category_id' => date('YmdHis')
        ]));

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function it_shows_product_detail()
    {
        $data = Product::factory()->make()->toArray();
        $product = Product::factory()->create($data);
        $response = $this->get(route('api.v1.products.show', $product->id));
        $response->assertOk();

        unset($data['user_id']);
        $response->assertJsonFragment($data);
        $response->assertJsonStructure([
            "data" => [
                "product" => [
                    'name',
                    'description',
                    'price',
                    'rating',
                    'category_id',
                    'category_name',
                ]
            ]
        ]);
    }
    /**
     * @test
     */
    public function it_does_not_show_product_detail_if_it_not_exists()
    {
        $response = $this->get(route('api.v1.products.show', date('YmdHis')));
        $response->assertStatus(404);
    }
}
