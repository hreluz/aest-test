<?php

namespace Tests\Feature\Api;

use App\Models\Category;

class CategoriesTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_can_create_a_category() {
        $data = Category::factory()->make()->toArray();
        $response = $this->post(route('api.v1.categories.store'), $data);
        $response->assertOk();

        $response->assertJsonFragment($data);
        $this->assertDatabaseHas('categories', $data);
    }

    /**
     * @test
     */
    public function it_doesnt_allow_to_create_category_with_empty_field()
    {
        $response = $this->post(route('api.v1.categories.store'), []);
        $response->assertStatus(422);
    }
    /**
     * @test
     */
    public function it_doesnt_allow_to_create_category_with_existing_name()
    {
        $name = 'something'.date('His');

        //Create category
        Category::factory()->create([
            'name' => $name
        ]);

        $data = Category::factory()->make([
            'name' => $name
        ])->toArray();

        $response = $this->post(route('api.v1.categories.store'), $data);
        $response->assertStatus(422);
    }
}
