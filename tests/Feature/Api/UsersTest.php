<?php

namespace Tests\Feature\Api;

use App\Models\User;

class UsersTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_creates_user()
    {
        $data = User::factory()->make()->toArray();
        $response = $this->post(route('api.v1.users.store'), $data);
        $response->assertOk();

        $response->assertJsonFragment($data);
        $this->assertDatabaseHas('users', $data);
    }

    /**
     * @test
     */
    public function it_doesnt_allow_to_create_user_with_empty_field()
    {
        $response = $this->post(route('api.v1.users.store'), []);
        $response->assertStatus(422);
    }
}
