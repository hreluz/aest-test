<?php


namespace Tests\Feature\Api;


use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BaseTestCase extends TestCase
{
    use DatabaseTransactions;

    protected function createVendor() {
        return User::factory()->create([
            'type' => User::TYPE_VENDOR
        ]);
    }

    protected function createBuyer() {
        return User::factory()->create([
            'type' => User::TYPE_BUYER
        ]);
    }
}
